#!/bin/sh

# Run on VM to bootstrap Foreman server
# Roland Stumpner - 05/2017

if ps aux | grep "/usr/share/foreman" | grep -v grep 2> /dev/null
then
    echo "Foreman appears to all already be installed. Exiting..."
else
    # Configure /etc/hosts file
#    echo "" | sudo tee --append /etc/hosts 2> /dev/null && \
#    echo "192.168.35.5    theforeman.example.com   theforeman" | sudo tee --append /etc/hosts 2> /dev/null

    # Update system first
    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get -y install ca-certificates
    # Install Base tools
    sudo apt-get install -y git
    sudo apt-get install -y curl
    sudo apt-get install -y links
    sudo apt-get install -y tree

    # Install the Puppet Agent
    wget https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
    sudo dpkg -i puppetlabs-release-pc1-xenial.deb

    # Install Foreman for CentOS 6
    echo "deb http://deb.theforeman.org/ xenial 1.15" > /etc/apt/sources.list.d/foreman.list
    echo "deb http://deb.theforeman.org/ plugins 1.15" >> /etc/apt/sources.list.d/foreman.list
    wget -q https://deb.theforeman.org/pubkey.gpg -O- | apt-key add -
    sudo apt-get update && apt-get -y install foreman-installer

    sudo foreman-installer

    # First run the Puppet agent on the Foreman host which will send the first Puppet report to Foreman,
    # automatically creating the host in Foreman's database
    sudo /opt/puppetlabs/puppet agent --test --waitforcert=60

    # Install some optional puppet modules on Foreman server to get started...
    sudo /opt/puppetlabs/puppet module install -i /etc/puppet/environments/production/modules puppetlabs-ntp
    sudo /opt/puppetlabs/puppet module install -i /etc/puppet/environments/production/modules puppetlabs-git
    sudo /opt/puppetlabs/puppet module install -i /etc/puppet/environments/production/modules puppetlabs-docker
fi

