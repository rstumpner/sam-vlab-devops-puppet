package {'apache2':
	ensure => present,
}

package {'php':
	ensure => present,
}

package {'libapache2-mod-php':
	ensure => present,
}

package {'links':
	ensure => present,
}

service {'apache2':
	ensure => running,
}


file {'/var/www/html/info.php':
        ensure => present,
        content => '<?php phpinfo(); ?>',
}
