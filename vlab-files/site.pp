node 'puppetclient.local' {
  class { 'apache': }             # use apache module
  apache::vhost { 'localhost':  # define vhost resource
    port    => '80',
    docroot => '/var/www/html'
  }
}

node default {
  file { '/etc/motd':
          content => "Managed by Puppet",
          ensure => 'present',
        }

}
