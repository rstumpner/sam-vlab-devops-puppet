node 'puppetserver.local' {
# Ensure Puppet Service Running
service { 'puppetserver':
  ensure => 'running',
  enable => 'true',
}

# Ensure PuppetDB Service Running
#service { 'puppetdb':
 # ensure => 'running',
 # enable => 'true',
#}
# Config Report Time
class { 'puppetdb':
         report_ttl => '30d',
         disable_cleartext => false,
         listen_address => '0.0.0.0',
       }
# Config Puppetmaster for PuppetDB
class { 'puppetdb::master::config':
         manage_report_processor => true,
         enable_reports  => true,
   }
}

node 'puppetdashboard.local' {
$ssl_dir = $::settings::ssldir
$puppetboard_certname = 'pupperserver.local' 
 
  class { 'apache': }
  class { 'apache::mod::wsgi': }
  class { 'puppetboard::apache::vhost':
 	vhost_name => 'puppetdashboard.local',
 	port       => 80,
  }
  class { 'puppetboard': 
	manage_virtualenv => true,
        puppetdb_host       => 'puppetserver.local',
        puppetdb_port       => 8080,
        puppetdb_ssl_verify => false,
#  	puppetdb_key        => "${ssl_dir}/private_keys/${puppetboard_certname}.pem",
#  	puppetdb_ssl_verify => "${ssl_dir}/certs/ca.pem",
#  	puppetdb_cert       => "${ssl_dir}/certs/${puppetboard_certname}.pem",
}
}

node 'puppetclient.local' {
  class { 'apache': }             # use apache module
  apache::vhost { 'localhost':  # define vhost resource
    port    => '80',
    docroot => '/var/www/html'
  }
}

node default {
  file { '/etc/motd':
          content => "Managed by Puppet",
          ensure => 'present',
        }

}
