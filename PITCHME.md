<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


## Puppet
* Devops Puppet
* Version 20180330

---
## Puppet vLAB Übersicht
* Voraussetzungen vLAB for Puppet
* Puppet Einführung
* Puppet First Steps
* Puppet Aufgaben Level 1
* Puppet Bolt
---
## Puppet Voraussetzungen vLAB (on-prem)
* 2 x CPU Core
* 4 GB RAM (minimal)
* 10 GB Space
* Installation of Virtualbox (https://www.virtualbox.org/)
---
## The vLAB Environment
* puppetclient.local
  * (Ubuntu 16.04 / Puppet Agent / Port 8081 )
* puppetagent-dev.local
  * (Ubuntu 16.04 / Puppet Agent / Port 8082)
* puppetserver.local
  * (Ubuntu 16.04 / Puppet Server)
* puppetdashboard.local
  * (Ubuntu 16.04 / Puppet Agent / 8083)
* puppetbolt.local
  * (Ubuntu 16.04 / Puppet bolt )

---
## Setup the Virtual Lab Environment
### (Vagrant)
* git clone https://www.gitlab.com/rstumpner/sam-vlab-devops-puppet
* cd sam-vlab-devops-puppet/vlab/vagrant/
* vagrant up
* vagrant status
* vagrant ssh puppetclient
---
## Einführung Puppet
* Ist ein Framework um Software und Konfigurationen zu verteilen
* Declerative DSL
* Based on Ruby
* Client / Server Architektur
  * Zertifikatsbasiert
  * Verschlüsselt
* Desired State Configuration (Make it so Prinzip)
* Policy-based configuration management
---
# Desired State Configuration
Zustände und Events

---
## Manual Installation of the Puppet Agent v4.8
### (puppetclient.local)

* Ubuntu 16.04 LTS Basis
* Install Puppet Agent:
	* wget https://apt.puppetlabs.com/puppetlabs-release-pc1-wheezy.deb
	* dpkg -i puppetlabs-release-pc1-wheezy.deb
	* apt-get update
	* apt-get install puppet-agent
---
## First Steps in Puppet
Login to puppetclient:
```bash
  vagrant ssh puppetclient
```
Check Puppet Agent Version:
```bash
  /opt/puppetlabs/bin/puppet agent --version
```
---
# Puppet Resources

Puppet Organisiert seine Konfigurationen in sogenannte Resourcen deren Eigenschaften den gewünschten Endzustand beschreiben

Der Aktuelle Zustand kann über den Puppet Agent abgefragt und gesetzt werden  

---
# Files
Welchen Zustand hat die Datei /etc/motd
```bash
/opt/puppetlabs/bin/puppet resource file /etc/motd
```
Ergebnis:
```bash
      file { '/etc/motd':
        ensure => 'absent',
      }
```
---
# Packages
Welchen Zustand hat das Software Paket CRON

```bash
/opt/puppetlabs/bin/puppet resource package cron
```
Ergebnis:
```
package { 'cron':
	ensure => 'present',
   }
```
---
# Services
Welchen Zustand hat der Service cron:

```bash
/opt/puppetlabs/bin/puppet resource service cron
```
Ergebnis:

```bash
      service { 'cron':
        ensure => 'running',
        enable => 'true',
      }
```

---
## My First Puppet RUN
Erstelle eine Datei mit folgendem Inhalt (first.pp):
```puppet
file { '/etc/motd':
        content => "Managed by Puppet",
        ensure => 'present',
      }
```


```bash
 sudo /opt/puppetlabs/bin/puppet apply /vlab-files/first.pp
```

```md
Notice: Compiled catalog for ubuntu-xenial.localdomain in environment production in 0.27 seconds
Notice: /Stage[main]/Main/File[/etc/motd]/ensure: defined content as '{md5}01fab9a9056afa3fdbd7bd9dd34a32ee'
Notice: Applied catalog in 0.04 seconds
```

---
## Facter
Um Informationen über das System zu bekommen oder deren Zustand zu beschreiben werden vom Puppet Agent sogenannte Facts erhoben.

* /opt/puppetlabs/bin/facter

```puppet
disks => {
  sda => {
    model => "HARDDISK",
    size => "10.00 GiB",
    size_bytes => 10737418240,
    vendor => "VBOX"
  },
  sdb => {
    model => "HARDDISK",
    size => "10.00 MiB",
    size_bytes => 10485760,
    vendor => "VBOX"
  }
```
---
## A LAMP Environment with Puppet

```
# lamp.pp

package {'apache2':
    ensure => present,
}

package {'php':
    ensure => present,
}

service {'apache2':
    ensure => running,
}

file {'/var/www/html/info.php':
        ensure => present,
        content => '<? phpinfo();?>',
}

package {'links':
    ensure => present,
}

```
---
## Aufgaben Puppet
#### Level 1

Installiere einen Web Application Stack mit einer Aplikation

##### Beispiel:
* LAMP
	* Kanboard (https://kanboard.org/)

---
## Puppet Server / Agent Architektur
![Puppet Architektur](_images/)

---
## Manual Installation of the Puppet Server (v4.8)
### (puppetserver.local)

* Ubuntu 16.04 LTS Basis
* Install Puppet Agent:
	* wget https://apt.puppetlabs.com/puppetlabs-release-pc1-wheezy.deb
	* dpkg -i puppetlabs-release-pc1-wheezy.deb
	* apt-get update
	* apt-get install puppetserver
  * sudo systemctl start puppetserver

---
## Verifying the puppetserver
* Checking Server Status
```md
 sudo systemctl status puppetserver
```
* Puppetish
```md
 puppet resource service puppetserver
```
* Checking Logfile of the Puppet Server
```md
tail /var/log/puppetlabs/puppetserver/puppetserver.log
```
* Enable Puppetserver for Startup
```bash
puppet resource service puppetserver enable=true
```
---
## Environments / Module  und Nodes

```md
/etc/puppetlabs/code/
├── environments
│   └── production
│       ├── environment.conf
│       ├── hieradata
│       ├── manifests
│       └── modules
└── modules
```
---
# Environments
Ein grundlegendes Konzept ist es in Puppet Verschiedene Umgebungen anlegen zu können. Am Agent kann die Umgebung konfiguriert werden.  

Default ist Production

---
# Module
Um Logik besser Wiederverwenden zu können wird Puppet Code in Module gekapselt.

---
# Hira
Bietet die Möglichkeit Konfiguration und Logik zu trennen .
Konfigurationsdaten werden in einer Hirachischen Struktur Abgebildet


Beispiel:
Suche eine Variable:
```bash
hiera ntp_server
hiera ntp_server --yaml web01.local.yaml
```
---
# Hira Config:

```
---
:backends:
  - yaml
:yaml:
  :datadir: "/etc/puppetlabs/code/environments/%{environment}/hieradata"
:hierarchy:
  - "nodes/%{::trusted.certname}"
  - common
```
---
# Hira Data
/etc/puppetlabs/code/environments/production/hieradata/nodes/puppetclient.local.yml

```
ntp::restrict:
  -
ntp::autoupdate: false
ntp::enable: true
ntp::servers:
  - 0.us.pool.ntp.org iburst
  - 1.us.pool.ntp.org iburst
  - 2.us.pool.ntp.org iburst
  - 3.us.pool.ntp.org iburst
```

---
# My First Manifest

/etc/puppetlabs/code/environment/manifests/site.pp

```puppet
node 'puppetclient.local' {
  file { '/etc/motd':
          content => "Managed by Puppet",
          ensure => 'present',
        }

}
```
Check Catalog Compile for the puppetclient
```md
puppet master --compile puppetclient.local
```
---
## Verifying the vLAB Environment
Checking the Client Installation
```bash
vaggrant ssh puppetclient
ping puppetserver.local

cat /etc/hosts

cat /etc/resolv.conf
```
Checking the Server Installation
```bash
vaggrant ssh puppetserver
ping puppetclient.local

cat /etc/hosts

cat /etc/resolv.conf

```

---
## Puppet Agent Config (puppetclient.local)

/etc/pupetlabs/puppet/puppet.conf:
```
[agent]
server = puppetserver.local
report = true
runinterval = 300
# masterport        = 8140
# environment = production
```
---
## Puppet RUN
Initiate a Puppet Agent Test RUN:
```md
sudo puppet agent --test
```
Debug:
```md
sudo puppet agent --test --server puppetserver.local
```
---
## Accept Client on Puppet Server
### (puppetserver.local)
Liste Zertifikate am Server
```md
sudo puppet cert --list
```
Erlaube alle gelisteten Zertifikate
```md
sudo puppet cert sign --all
```
---
## Puppet Agent Rerun
### (puppetclient.local)
Initiate a Puppet Agent Test RUN:
```md
sudo puppet agent --test
```
Debug:
```md
sudo puppet agent --test --server puppetserver.local
```
---
## Reuse Modules from Puppetforge
### (puppetserver.local)

https://forge.puppet.com/

* Installl a Puppet Module
```bash
sudo /opt/puppetlabs/bin/puppet module install -i /etc/puppetlabs/code/environments/production/modules/ puppetlabs-apache
```
* Apply Module to a Node

sudo vi /etc/puppetlabs/code/environments/production/manifests/site.pp

```puppet
node 'puppetclient.local' {
  class { 'apache': }             # use apache module
  apache::vhost { 'localhost':  # define vhost resource
    port    => '80',
    docroot => '/var/www/html'
  }
}
```

---
## Puppet Agent Rerun
### (puppetclient.local)
Initiate a Puppet Agent Test RUN:
```md
sudo puppet agent --test
```
Test the Puppet Run
```
links http://localhost
```

---
# Aufgaben Puppet
### Level 2
* Installation von Puppetboard mit Puppetserver
---
## Install PuppetDB (puppetserver.local)

Installation PuppetDB Modul

```bash
sudo /opt/puppetlabs/bin/puppet module install -i /etc/puppetlabs/code/environments/production/modules/ puppetlabs-puppetdb
```

Add PuppetDB Config to Manifest (site.pp):
```puppet
node 'puppetserver.local' {
# Ensure Puppet Service Running
service { 'puppetserver':
  ensure => 'running',
  enable => 'true',
}

# Config Report Time
class { 'puppetdb':
         report_ttl => '30d',
         disable_cleartext => false,
         listen_address => '0.0.0.0',
       }

class { 'puppetdb::master::config':
         manage_report_processor => true,
         enable_reports  => true,
   }
}
```
---
## Installation Puppetboard (puppetserver.local)

* Install Dependencies
```bash
puppet module install -i /etc/puppetlabs/code/environments/production/modules/ puppet/puppetboard
```
Add Puppetdashboard Config to Manifest (site.pp):
```puppet
node 'puppetdashboard.local' {
  class { 'puppetboard': }
    manage_virtualenv => true,
    puppetdb_host       => 'puppetserver.local',
    puppetdb_port       => 8080,
    puppetdb_ssl_verify => false,

}
```
---
# Aufgaben Level 4
* Mein erstes Puppet Modul
* Puppet und Templates
---
# My First Puppet Module

/etc/puppetlabs/code/environment/modules/

puppet module generate


Links:
https://puppet.com/docs/puppet/5.5/modules_fundamentals.html
https://puppet.com/docs/puppet/5.5/bgtm.html
---
Logging from Puppet Modules:

notice("The value is: ${yourvar}")
notify{"The value is: ${yourvar}": }


---
# Puppet with Templates

file { 'motd':
    path    => '/etc/motd',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('motd.erb')
  }

  file { 'motd':
      path    => '/etc/motd',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => inline_template('<%variable%>')
    }

---
## Puppet Bolt

* Task based System for "one" Time Execution
* no Puppet Agent needed
* no Puppet DSL needed

---
## Puppet Bolt Basics
* bold command
* bold file upload
* bolt script
* bolt task

---
## Puppet Bolt Installation
	```
	bash gem install bolt
	```

---
# First Steps in bolt
	* bolt command run 'netstat -an' -n localhost -u puppetbolt -p puppetbolt -k

---
# Bolt Upload Files
	* bolt file upload /vlab-files/first-remote-exec.sh /home/puppetbolt/first-remote-exec.sh -n puppetclient.local -u puppetbolt -p puppetbolt -k

---
# Bolt Run Script
	* bolt script run /vlab-files/first-remote-exec.sh -n puppetclient.local,puppetserver.local -u puppetbolt -p puppetbolt -k

```bash
#!/bin/bash
echo " Hello from Puppet Bolt"
```
---
# Bolt Run Tasks

```bash
# modules/gitrepo/tasks/git_clone
#!/usr/bin/env bash
if [ -z "$PT_apppath" ]; then
  apppath=$PT_apppath
else
  apppath='/bin/'
fi
#pushd $apppath
  touch /home/puppetbolt/touchme
#popd
```

	* bolt task run gitrepo::git_clone --nodes puppetclient.local --modulepath /vlab-files/modules -u puppetbolt -p puppetbolt -k
---
# Puppet Enterprise
* Puppet Enterprise Console
  * Razor (onBoarding)
  * MCollective (Metrics)
* Puppet Discovery
  * Inventory Console
* Puppet Pipelines
  * CI/CD
* Links:
  * Puppet Learning VM
  https://puppet.com/download-learning-vm?_ga=2.203291217.1996469522.1521805030-2119767075.1520268476
  * Puppet Discovery
  https://www.youtube.com/watch?v=6_T53JmzVBU
---
# Puppet Cheat Sheet
* puppet Manifest local execute (puppet apply manfest.pp)
* puppet Test Run (puppet apply --test --server --report --certname )
* puppet agent Debug (puppet --debug --verbose)
* puppet test code (puppet --parseonly)
* puppet config (puppet config print server puppet)
* delete puppet agent Zertifikate (rm -Rf /home/ubuntu/.puppetlabs/etc/puppet/ssl/* )
---
# Puppet Bolt Cheat Sheet
* bolt command run 'echo Hello from Bolt' -u puppetbolt -p puppetbolt -k
---
# Links
* Puppetlabs http://www.pupetlabs.com
* Getting Started Guide https://www.digitalocean.com/community/tutorials/getting-started-with-puppet-code-manifests-and-modules
* Puppetconf17 Introduction to Tasks on Youtube from Yasmin Rajabi (https://www.youtube.com/watch?v=si3h3WCgd9k)
* Puppet Tasks https://puppet.com/blog/easily-automate-ad-hoc-work-new-puppet-tasks
* Puppet Self Paced Labs https://puppetlabs.learndot.com/public/courses/7422-introduction-to-bolt
*
https://puppet.com/docs/bolt/0.x/running_tasks_and_plans_with_bolt.html
* Puppet bolt Example42
https://www.example42.com/2017/10/16/plans_and_tasks/
* Puppet Learning VM
https://puppet.com/download-learning-vm?ls=Outbound&lsd=Email&cid=7010f0000017Qey&utm_medium=email&utm_campaign=Q4FY18_EMEA_SEMEA_DEMAND_EM_BLST_Cisco-Live-Barcelona-emails&utm_source=email-2018-02&utm_content=cisco-live-barcelona-emails&mkt_tok=eyJpIjoiTjJFeU5HWTJNVE13TlRBMiIsInQiOiJFN3VvN1g0UTRwT0ZKeGo3VU1tZVRLOFpaNWlzd2Iwa3B0UWV5K1B0UEx3N3FIQ0FYeDJBRW5rZlEyNDJPSlFZSDd3cTF5YUw0NHc4RTVwTWlka2JiZWFBYmhuZTU0WTNqeGo0bEx2SjgzOXYxSGd6T3MwckJmZENnVmxZeWhBVyJ9
* Puppet Beginners Guide for Module Programming
  https://puppet.com/docs/puppet/5.5/bgtm.html
* Puppet Hira Example
  https://puppet.com/docs/hiera/3.2/complete_example.html
---
# Begriffserklärungen
* Domain Specific Language (DSL)
    * https://de.wikipedia.org/wiki/Dom%C3%A4nenspezifische_Sprache
* policy-based configuration management
* Configuration Management
  * https://en.wikipedia.org/wiki/Configuration_management
* Desired State Configuration
* Promise Theory
  * https://en.wikipedia.org/wiki/Promise_theory  
---
# Notizen
<!---
--->
