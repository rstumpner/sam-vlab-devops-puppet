package {'apache2':
	ensure => present,
}

package {'php':
	ensure => present,
}

package {'mariadb-server':
	ensure => present,
}

package {'links':
	ensure => present,
}

service {'apache2':
	ensure => running,
}

service {'mysqld':
	ensure => running,
}

file {'/var/www/html/info.php':
        ensure => present,
        content => '<? phpinfo();?>',
}
