<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->

----
# Systemadministraton 2 Übung
* SAM 2 Übung
* Devops


---
# Puppet
* Devops Puppet
* Version 20161229

---
# Puppet Übung Übersicht
* Voraussetzungen vLAB
* Docker Einführung
* Docker First Steps
* Docker Aufgaben
* Puppet Bolt a Task based Addion
---
# Puppet Brainstorming
* Docker Lösung
* Using Docker
* Docker Deployment
* Create your own Repo
* Versionierung

---
# Voraussetzungen vLAB Puppet
* Minimal:
	* Virtualbox
	* Ubuntu 16.04
	* 2 GB RAM
	* Ruby 1.9.3 / 2.0.x / 2.1.x

* Recomended
	* Virtualbox
	* Ubuntu 16.04
	* 2GB RAM
	* Ruby 2.1.x
	* Vegrant

---
# Einführung Puppet
* Ist ein Framework um Software Auszurollen
* Desired State Configuration (Make it so)
* Declerative DSL
* Based on Ruby
* Client / Server Architektur

---
# Power up Puppet vLAB (Vagrant)
* git clone https://www.gitlab.com/stumpi/
* cd vlab/vegrant-basic/
* vagrant up
* vagrant ssh
* Username: ubuntu
* Password: ubuntu

---
# Installation Puppet v4.8(Manual)
* Ubuntu 16.04 LTS Basis
* Install Puppet Agent:
	* wget https://apt.puppetlabs.com/puppetlabs-release-pc1-wheezy.deb
	* dpkg -i puppetlabs-release-pc1-wheezy.deb
	* apt-get update
	* apt-get install puppet-agent

---
# First Steps in Puppet
* Check Puppet Agent Version:
	* /opt/puppetlabs/bin/puppet agent --version
* Puppet Resources
	* Packages /opt/puppetlabs/bin/puppet resource package cron
	```md
      package { 'cron':
         ensure => 'present',
       }
   ```
   * Files /opt/puppetlabs/bin/puppet resource file /etc/motd
   ```md
      file { '/etc/motd':
        ensure => 'absent',
      }
    ```
   * Services /opt/puppetlabs/bin/puppet resource service cron
   ```md
      service { 'cron':
        ensure => 'running',
        enable => 'true',
      }
   ```

---
# First Puppet Catalog
* first.pp
```md
file { '/etc/motd':
        content => "Managed by Puppet",
        ensure => 'present',
      }
```
* sudo /opt/puppetlabs/bin/puppet apply first.pp
```md
Notice: Compiled catalog for ubuntu-xenial.localdomain in environment production in 0.27 seconds
Notice: /Stage[main]/Main/File[/etc/motd]/ensure: defined content as '{md5}01fab9a9056afa3fdbd7bd9dd34a32ee'
Notice: Applied catalog in 0.04 seconds
```
---
# Facter
* /opt/puppetlabs/bin/facter
```md
aio_agent_version => 1.8.2
augeas => {
  version => "1.4.0"
}
disks => {
  sda => {
    model => "HARDDISK",
    size => "10.00 GiB",
    size_bytes => 10737418240,
    vendor => "VBOX"
  },
  sdb => {
    model => "HARDDISK",
    size => "10.00 MiB",
    size_bytes => 10485760,
    vendor => "VBOX"
  }
}
dmi => {
  bios => {
    release_date => "12/01/2006",
    vendor => "innotek GmbH",
    version => "VirtualBox"
  },
  board => {
    manufacturer => "Oracle Corporation",
    product => "VirtualBox"
  },
  chassis => {
    type => "Other"
  },
  manufacturer => "innotek GmbH",
  product => {
    name => "VirtualBox"
  }
}

```
---
# Puppet Aufgaben
* Setup a LAMP Environment with Puppet
* Install Kanboard as LAMP App

---
# Puppet Lösung (LAMP)
* Install Apache2 Package
```md
package {'apache2':
	ensure => present,
}

package {'php':
	ensure => present,
}

package {'mariadb-server':
	ensure => present,
}

package {'links':
	ensure => present,
}

service {'apache2':
	ensure => running,
}

service {'mysqld':
	ensure => running,
}

file {'/var/www/html/info.php':
        ensure => present,
        content => '<? phpinfo();?>',
}


```
---
# Puppet Modules
*
---
# Puppet Bolt
* How puppet bolt works
	* Task based System for "one" Time Execution
	* no Puppet Agent needed
	* no Puppet DSL
---
* Installation
	* gem install bolt
---
* First Steps
	* bolt command run 'netstat -an' -n localhost -u ubuntu -p ubuntu
---
* links
	* Puppetconf17 on Youtube from Yasmin Rajabi (https://www.youtube.com/watch?v=si3h3WCgd9k)

---
# Docker Container Playground (Whalesay)
* Is a Container Persistant ???
* touch test
* touch test123
* Commit an Image
* docker run -it ubuntu bash
* sudo apt-get install apache2
* Notiere Container ID: 9a0874f113e6
* sudo docker commit -m "Added Apache2" -a "Roland Stumpner" 9a0874f113e6 roland/ubuntu-apache2-manual
* sudo docker run -p 80:80 -t -i roland/ubuntu-apache2-manual /bin/bash
*
---
# Docker Theory
* Skizze Layerd Filesystem
* Skizze Ökosystem / Repository
* Stateless
---
# Docker Aufgaben
* Setup a LAMP Environment with Docker
* Create a Docker Container for Kanboard
---
# Docker Lösung
* mkdir Firstcontainer
* touch Dockerfile
* Edit Dockerfile:
 FROM ubuntu:latest
 RUN apt-get -y update && apt-get install -y apache2
 CMD service apache2 start
* sudo docker build -t ubuntu-apache2 .
---
# Docker Lösung
*
---
# Docker Cheat Sheet
* List Images (docker images)
* Run Latest Ubuntu Image (sudo docker run -t -i ubuntu:latest /bin/bash)
* Download a new Container ( docker pull kanoard\kanboard
* Show Running Containers (sudo docker ps)
* Stop a Container (sudo docker stop kanoard)
* Remove Containers not running (docker rm `docker ps -aq -f status=exited`)
* Mount a Directory from Docker Host to Container (sudo docker run -d -p 80:80 -v /home/roland/Dokumente/privat/hausbau/kanboard/data:/var/www/app/data -t kanboard/kanboard:latest)
*
---
# Notizen
 docker run hello-world
    2  service docker start
    3  sudo service docker start
    4  docker run hello-world
    6  docker run -it ubuntu bash
    7  sudo docker run -it ubuntu bash
    8  docker status
    9  docker --help
   10  docker info
   11  sudo docker info
   12  sudo docker info help
   13  sudo docker info --help
   14  sudo docker info
   15  docker help
   16  docker ps
   17  sudo docker ps
   18  sudo docker info
   19  sudo docker help
   20  sudo docker stats
   21  sudo docker images
   22  ls
   23  sudo docker info
   24  sudo docker help
   25  sudo docker ps
   26  sudo docker stop
   27  sudo docker info
   28  sudo docker stop hello-world
   29  sudo docker logs
   30  sudo docker logs hello-wordl
   31  sudo docker logs hello-world
   32  ps -awx
   33  sudo docker images
   34  histoy
   35  history
   36  sudo docker help
   37  sudo docker run -it ubuntu bash
   38  sudo docker run ubuntu
   39  sudo docker start ubuntu
   40  sudo docker start
   41  sudo docker run ubuntu
   42  sudo docker info
   43  sudo docker ps
   44  sudo docker start ubuntu
   45  exit
